const createError = require('http-errors')
const express = require('express')
const logger = require('morgan')
const mailer = require('./utils/mailer')
const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// Route for mailer service
app.post('/mail', async function (req, res, next) {
  try {
    let info = await mailer.sendMail(req.body)
    res.json(info)
  } catch (e) {
    console.log(e)
    res.json(e)
  }
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.sendStatus(err.status || 500)
})

module.exports = app
