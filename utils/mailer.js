const nodemailer = require('nodemailer')

function sendMail(options) {
  console.log('Run mailer')
  console.log(options)
  return new Promise(function (resolve, reject) {
    if (!options.subject || !options.text || !options.auth || !options.info) {
      reject('Not enough information')
    }
    let {subject, text, auth, info, html} = {...options},
      transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: auth.MAIL_USERNAME,
          pass: auth.MAIL_PASSWORD
        }
      }),
      mailOptions = {
        from: info.MAIL_FROM, // sender address
        to: info.MAIL_TO.split(','), // list of receivers
        subject: subject,
        text: text,
        html: html || ''
      }


    transporter.sendMail(mailOptions, function (err, info) {
      if(err)
        reject(err)
      else
        resolve(info)
    })
  })
}

module.exports = {
  sendMail: sendMail
}
